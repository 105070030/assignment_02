var loadState = {
    preload: function () {
    // Add a 'loading...' label on the screen
    var loadingLabel = game.add.text(game.width/2, 150, 'loading...', { font: '30px future', fill: '#ffffff'});
    loadingLabel.anchor.setTo(0.5,0.5);
    
    // Display the progress bar
    var progressBar = game.add.sprite(game.width /2, 200, 'progressBar');
    progressBar.anchor.setTo(0.5,0.5);
    game.load.setPreloadSprite(progressBar);
    
    // Loat game sprites.
    game.load.atlasXML('sheet', 'assets/sheet.png', 'assets/sheet.xml');
    game.load.atlasXML('blueUI', 'assets/blueSheet.png', 'assets/blueSheet.xml');
    game.load.atlasXML('stoneUI', 'assets/stoneSheet.png', 'assets/stoneSheet.xml');

    game.load.image('menu', 'assets/background.png');
    game.load.image('background', 'assets/black.png');
    game.load.image('panel', 'assets/panel.png');
    game.load.image('pixelP', 'assets/pixelP.png');
    game.load.image('pixelE', 'assets/pixelE.png');
    game.load.image('gameOver', 'assets/gameOver.png');
    game.load.image('settingPanel', 'assets/settingPanel.png');
    game.load.image('highScore', 'assets/highScore.png');
    game.load.image('record', 'assets/record.png');
    game.load.image('rankBoard', 'assets/rankBoard.png');
    game.load.image('Boss', 'assets/Boss.png');
    

    game.load.spritesheet('player', 'assets/player.png', 99, 75);
    game.load.spritesheet('buttonRestart', 'assets/buttonRestart.png', 190, 49);
    game.load.spritesheet('buttonHome', 'assets/buttonHome.png', 190, 49);
    game.load.spritesheet('buttonRank', 'assets/buttonRank.png', 190, 49);
    game.load.spritesheet('hitRed', 'assets/hitRed.png', 48, 48);
    game.load.spritesheet('hitBlue', 'assets/hitBlue.png', 48, 48);
    game.load.spritesheet('super', 'assets/super.png', 33, 33);
    game.load.spritesheet('setting', 'assets/setting.png', 32, 35);
    game.load.spritesheet('volumnUp', 'assets/volumnUp.png', 22, 20);
    game.load.spritesheet('volumnDown', 'assets/volumnDown.png', 22, 20);
    game.load.spritesheet('close', 'assets/close.png', 15, 15);

    game.load.text('waves', 'assets/waves.json');

    //load audio
    game.load.audio('laser1', 'assets/audio/sfx_laser1.ogg');
    game.load.audio('laser2', 'assets/audio/sfx_laser2.ogg');
    game.load.audio('lose', 'assets/audio/sfx_lose.ogg');
    game.load.audio('click', 'assets/audio/click.ogg');
    game.load.audio('error', 'assets/audio/error.ogg');
    game.load.audio('BGM', 'assets/audio/BGM.ogg');
    game.load.audio('newHigh', 'assets/audio/newScore.ogg');
    game.load.audio('skillshot', 'assets/audio/skillshot.ogg');
    game.load.audio('accend', 'assets/audio/accend.ogg');
    
    //load font
    //game.load.bitmapFont('myfont', 'assets/kenvector_future.ttf');

    },
    create: function() {
    // Go to the menu state
    game.state.start('menu');
    }
};