var endState = {
    create: function(){

        this.click = game.add.audio('click');

        //Add a background image
        game.add.image(0,0,'menu');

        this.panel = game.add.image(game.width/2, game.height/2, 'gameOver');
        this.panel.anchor.setTo(0.5, 0.5);

        var nameLabel = game.add.text(game.width/2, game.height/2-140, 'Game Over', {font:'60px future', fill: '#ffffff'});
        nameLabel.anchor.setTo(0.5,0.5);
        nameLabel.stroke = '#B1A077';
        nameLabel.strokeThickness = 10;

        //Show the score at the center of the screen
        var scoreLabel = game.add.text(game.width/2, game.height/2-85, 'score: ' + game.global.score, {font: '25px future', fill: '#ffffff'});
        scoreLabel.anchor.setTo(0.5, 0.5);
        

        // add button
        this.restartBtn = game.add.button(game.world.centerX - 95, 300, 'buttonRestart', this.restart, this, 0, 0, 1);
        this.homeBtn = game.add.button(game.world.centerX - 95, 360, 'buttonHome', this.quit, this, 0, 0, 1);
        this.rankBtn = game.add.button(game.world.centerX - 95, 420, 'buttonRank', this.leaderBoard, this, 0, 0, 1);

        this.update = false;
        this.rankLoaded = false;

        firebase.database().ref('rank').once('value').then(function (snapshot) {
            game.global.firstscore = snapshot.child('first/score').val();
            game.global.secondscore = snapshot.child('second/score').val();
            game.global.thirdscore = snapshot.child('third/score').val();
        }).then(()=>{
            if(game.global.score > game.global.thirdscore){
                this.highScore = game.add.image(game.world.centerX + 105, 430, 'highScore');
                this.newHigh = game.add.audio('newHigh');
                this.newHigh.play();
                this.record();
            }else{
                this.update = true;
            }
        });
        
    },
    record: function() {
        this.restartBtn.inputEnabled = false;
        this.homeBtn.inputEnabled = false;
        this.rankBtn.inputEnabled = false;
        this.restartBtn.alpha = 0.2;
        this.homeBtn.alpha = 0.2;
        this.rankBtn.alpha = 0.2;
        this.namePanel = game.add.image(game.width/2, 314, 'record');
        this.namePanel.anchor.setTo(0.5,0.5);
        this.namePanel.scale.setTo(0,0);
        game.add.tween(this.namePanel.scale).to( { x: 1, y: 1 }, 500).start();

        /// input event
        this.input = '';
        game.input.keyboard.addCallbacks(this, null, null, this.keyPress);
        this.enter = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        this.enter.onDown.add(this.submit, this);
        this.nameLabel = game.add.text(game.width/2, 330, this.input, {font: '25px future', fill: '#ffffff'});
        this.nameLabel.anchor.setTo(0.5,0.5);
    },
    submit: function() {
        if(this.input != ''){
            game.input.keyboard.reset();
            this.enter.onDown.remove(this.submit, this);
            this.restartBtn.inputEnabled = true;
            this.homeBtn.inputEnabled = true;
            this.rankBtn.inputEnabled = true;
            this.restartBtn.alpha = 1;
            this.homeBtn.alpha = 1;
            this.rankBtn.alpha = 1;
            this.nameLabel.destroy();
            this.namePanel.destroy();

            if (game.global.score > game.global.firstscore){
                firebase.database().ref('rank/first').update({
                    name: this.input,
                    score: game.global.score
                }).then(()=>{this.update = true;});
            }else if(game.global.score > game.global.secondscore){
                firebase.database().ref('rank/second').update({
                    name: this.input,
                    score: game.global.score
                }).then(()=>{this.update = true;});
            }else{
                firebase.database().ref('rank/third').update({
                    name: this.input,
                    score: game.global.score
                }).then(()=>{this.update = true;});
            }
        }
    },
    keyPress: function(char){
        if(this.input.length < 8){
            this.input += char;
            this.nameLabel.setText(this.input);
        }
    },
    restart: function() {
        this.click.play();
        game.input.keyboard.reset();
        game.input.keyboard.start();
        // Start the actuall game
        game.state.start('play');
    },
    quit: function() {
        this.click.play();
        // Start the actuall game
        game.state.start('menu');
    },
    leaderBoard: function() {
        if(!this.update){return;}
        this.click.play();
        this.restartBtn.inputEnabled = false;
        this.homeBtn.inputEnabled = false;
        this.rankBtn.inputEnabled = false;
        this.restartBtn.alpha = 0.2;
        this.homeBtn.alpha = 0.2;
        this.rankBtn.alpha = 0.2;
        if(!this.rankLoaded){
            this.rankBoard = game.add.image(game.width/2, 350, 'rankBoard')
            this.rankBoard.anchor.setTo(0.5,0.5);
            firebase.database().ref('rank').once('value').then(function (snapshot) {
                firstS = snapshot.child('first/score').val();
                firstN = snapshot.child('first/name').val();
                secondS = snapshot.child('second/score').val();
                secondN = snapshot.child('second/name').val();
                thirdS = snapshot.child('third/score').val();
                thirdN = snapshot.child('third/name').val();
            }).then(() => {
                this.rankLoaded = true;
                this.Firstn = game.add.text(game.width/2-50, 312, firstN, {font: '20px future', fill: '#ffffff'});
                this.Secondn = game.add.text(game.width/2-50, 352, secondN, {font: '20px future', fill: '#ffffff'});
                this.thirdn = game.add.text(game.width/2-50, 392, thirdN, {font: '20px future', fill: '#ffffff'});
                this.Firsts = game.add.text(game.width/2+40, 312, firstS, {font: '25px future', fill: '#ffffff'});
                this.Seconds = game.add.text(game.width/2+40, 352, secondS, {font: '25px future', fill: '#ffffff'});
                this.Thirds = game.add.text(game.width/2+40, 392, thirdS, {font: '25px future', fill: '#ffffff'});
            });
            this.close = game.add.button(game.width/2+100, 255, 'close', function(){
                this.click.play();
                this.rankBoard.visible = false;
                this.Firstn.visible =  false;
                this.Secondn.visible = false;
                this.thirdn.visible = false;
                this.Firsts.visible = false;
                this.Seconds.visible = false;
                this.Thirds.visible =  false;
                this.close.visible = false;
                this.close.inputEnabled = false;
                this.restartBtn.inputEnabled =true;
                this.homeBtn.inputEnabled = true;
                this.rankBtn.inputEnabled = true;
                this.restartBtn.alpha = 1;
                this.homeBtn.alpha = 1;
                this.rankBtn.alpha = 1;
            },this,0,0,1);
        }else{
            this.rankBoard.visible = true;
            this.Firstn.visible =  true;
            this.Secondn.visible = true;
            this.thirdn.visible = true;
            this.Firsts.visible = true;
            this.Seconds.visible = true;
            this.Thirds.visible =  true;
            this.close.visible = true;
            this.close.inputEnabled = true;
        }
    }
}