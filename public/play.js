var playState = {
    preload: function() {
    },
    create: function() {
        game.stage.backgroundColor = '#3498db';

        /// Add background
        this.background = game.add.group();
        this.background.enableBody = true;
        for(var i=0; i<4;i++){
            for(var j=0; j<4;j++){
                var backFrag = game.add.sprite(256*i, -256+255*j, 'background');
                this.background.add(backFrag);
                backFrag.checkWorldBounds = true;
                backFrag.events.onOutOfBounds.add(function(back){back.y = -256;}, this);
                backFrag.body.velocity.y = 20;
            }
        }

        game.global.score = 0;

        /// parse JSON
        this.wavesData = JSON.parse(game.cache.getText('waves'));

        /// create audio
        this.BGM = game.add.audio('BGM', 0.5);
        this.BGM.play();
        this.laser1 = game.add.audio('laser1');
        this.laser2 = game.add.audio('laser2');
        this.lose = game.add.audio('lose');
        this.click = game.add.audio('click');
        this.error = game.add.audio('error');
        this.skillshot = game.add.audio('skillshot');
        this.accend = game.add.audio('accend');

        game.physics.startSystem(Phaser.Physics.ARCADE);

        game.renderer.renderSession.roundPixels = true;

        /// keyboard events
        this.cursor = game.input.keyboard.createCursorKeys();
        this.shootKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        this.shootKey.onDown.add(this.playerShoot, this);
        this.skillKey = game.input.keyboard.addKey(Phaser.Keyboard.C);
        this.skillKey.onDown.add(this.reduceEnergy, this);
        
        /// Add player
        this.player = game.add.sprite(game.width/2, game.height/2, 'player');
        this.player.animations.add('super', [0,1,2], 8, true);
        this.player.anchor.setTo(0.5,0.5);
        this.player.scale.setTo(0.5,0.5);
        this.player.inHurt = false;

        /// Add player shoot
        this.playerbeam = game.add.group();
        this.playerbeam.enableBody = true;
        this.playerbeam_2 = game.add.group();
        this.playerbeam_2.enableBody = true;

        /// Add enemy shoot
        this.rate = 0.6;
        this.enemybeam_1 = game.add.group();
        this.enemybeam_1.enableBody = true;
        game.time.events.loop(3000, this.enemyShoot_1, this);
        game.time.events.loop(5000, this.enemyShoot_1, this);

        // Add skill shot
        this.multiShot = game.add.group();
        this.multiShot.enableBody = true;

        // add hit
        this.hits = game.add.group();
        this.hits_enemy = game.add.group();

        /// Add enemies group
        this.enemies = game.add.group();
        this.enemies.enableBody = true;
        game.time.events.loop(8000, this.addEnemy, this);
        this.wave = 0;

        /// Add enemies group
        this.enemies_1 = game.add.group();
        this.enemies_1.enableBody = true;
        game.time.events.loop(8000, this.addEnemy_1, this);

        /// Add boss
        this.Boss = game.add.sprite(game.width/2, -160, 'Boss');
        this.Boss.anchor.setTo(0.5,1);
        this.BossDir = 1;
        this.BossLife = 50;

        // super
        var interval = game.rnd.integerInRange(25, 40);
        game.time.events.add(interval*1000, this.super, this);
        // energy
        var interval_2 = game.rnd.integerInRange(15, 30);
        game.time.events.add(interval_2*1000, this.energyDrop, this);

        /// Particle
        this.emitter = game.add.emitter(422, 320, 15);
        this.emitter.makeParticles('pixelE');
        this.emitter.setYSpeed(-150, 150);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(2, 0, 2, 0, 800);
        this.emitter.gravity = 500;

        this.emitter_P = game.add.emitter(422, 320, 15);
        this.emitter_P.makeParticles('pixelP');
        this.emitter_P.setYSpeed(-150, 150);
        this.emitter_P.setXSpeed(-150, 150);
        this.emitter_P.setScale(2, 0, 2, 0, 800);
        this.emitter_P.gravity = 500;

        /// UI
        this.panel = game.add.sprite(0,540, 'panel');
        this.settingPanel = game.add.sprite(590,335, 'settingPanel');

        this.volumeLabel = game.add.text(690, 360, this.BGM.volume*10, {font:'22px future', fill: '#ffffff'});
        this.volumeLabel.anchor.setTo(0.5,0);
        this.volumeLabel.visible = false;

        this.volumnUp = game.add.button( 730, 360, 'volumnUp',this.volumeU, this, 0, 0, 1);
        this.volumnDown = game.add.button( 628, 360, 'volumnDown', this.volumeD, this, 0, 0, 1);

        this.settingPanel.visible = false;
        this.volumnUp.visible = false;
        this.volumnDown.visible = false;
        this.panel.enableBody = true;
        this.lifeLabel = game.add.text(10, 550, 'Life: ', {font:'22px future', fill: '#ffffff'});
        this.life = 10;
        this.scoreLabel = game.add.text(10, 578, 'Score: ' + game.global.score, {font:'15px future', fill: '#ffffff'});
        //Life
        this.playerLife = game.add.group();
        for(var i = 0; i < this.life; i++){
            this.playerLife.add(game.add.sprite( 85+40*i, 550, 'sheet', 'playerLife1_red.png'));
        }
        //Ultimate skill
        this.playerEnergy = game.add.group();
        this.energy = 0;
        

        this.isPause = false;
        this.settingtBtn = game.add.button( 750, 550, 'setting', this.pause, this, 1, 0, 2);
        

        game.physics.arcade.enable(this.player);
        game.physics.arcade.enable(this.panel);
        game.physics.arcade.enable(this.Boss);

    },
    volumeU: function() {
        if(this.BGM.volume<=0.8){
            this.click.play();
            this.BGM.volume += 0.1;
            this.volumeLabel.setText(Phaser.Math.roundTo(this.BGM.volume*10, 0));
        }else{
            this.error.play();
        }
        
    },
    volumeD: function() {
        if(this.BGM.volume>=0.1){
            this.click.play();
            this.BGM.volume -= 0.1;
            this.volumeLabel.setText(Phaser.Math.roundTo(this.BGM.volume*10, 0));
        }else{
            this.error.play();
        }
    },
    inSuper: function(player, buff) {
        buff.destroy();
        this.player.animations.play('super');
        this.player.inHurt = true;
        this.autoShoot = game.time.events.loop(200, this.playerShoot, this);
        this.autoShoot_2 = game.time.events.loop(300, this.playerShoot_2, this);
        game.time.events.add(15000, function() {
            game.time.events.remove(this.autoShoot);
            game.time.events.remove(this.autoShoot_2);
            this.player.animations.stop();
            this.player.frame = 0;
            this.player.inHurt = false;
        }, this);
    },
    super: function() {
        ///Add SuperBuff
        this.superBuff = game.add.sprite(game.width/2, 0, 'super');
        this.superBuff.anchor.setTo(0.5,1);
        game.physics.arcade.enable(this.superBuff);
        this.superBuff.body.velocity.y = 100;
        this.superBuff.body.velocity.x = game.rnd.pick([1,-1]) * 10;
        this.superBuff.animations.add('anime', [0,1,2], 8, true);
        this.superBuff.animations.play('anime');
        var interval = game.rnd.integerInRange(25, 40);
        game.time.events.add(interval*1000, this.super, this);
        this.superBuff.checkWorldBounds = true;
        this.superBuff.events.onOutOfBounds.add(function() {
            console.log("super destoy");
            this.superBuff.destroy();
        }, this);
    },
    energyDrop: function() {
        ///Add SuperBuff
        this.energyBuff = game.add.sprite(game.width/2, 0, 'sheet', 'powerupYellow_bolt.png');
        this.energyBuff.anchor.setTo(0.5,1);
        game.physics.arcade.enable(this.energyBuff);
        this.energyBuff.body.velocity.y = 100;
        this.energyBuff.body.velocity.x = game.rnd.pick([1,-1]) * 15;
        var interval = game.rnd.integerInRange(15, 30);
        game.time.events.add(interval*1000, this.energyDrop, this);
        this.energyBuff.checkWorldBounds = true;
        this.energyBuff.events.onOutOfBounds.add(function() {
            console.log("energy destoy");
            this.energyBuff.destroy();
        }, this);
    },
    addBoss: function() {
        game.physics.arcade.enable(this.Boss);
        this.bossBeam = game.add.group();
        this.bossBeam.enableBody = true;
        game.add.tween(this.Boss.body).to( { y: 60 }, 3000, null, true);
        game.time.events.loop(3000, this.BossShoot, this);
    },
    BossShoot: function() {
        this.laser1.play();
        for(var i=0; i<5; i++){
            var beam = this.bossBeam.getFirstDead();
            if(!beam){
                beam = game.add.sprite(this.Boss.x, this.Boss.y, 'sheet', 'meteorBrown_small2.png');
                this.bossBeam.add(beam);
            }
            beam.anchor.setTo(0.5, 0);
            beam.reset(this.Boss.x, this.Boss.y);
            beam.body.velocity.x = -200+i*100;
            if(i<=2){
                beam.body.velocity.y = 100+i*100;
            }else{
                beam.body.velocity.y = 400-i*80;
            }
            beam.checkWorldBounds = true;
            beam.outOfBoundsKill = true;
        }
        if(game.rnd.frac() >= 0.1){
            console.log('shoot');
            this.Boss.body.velocity.x = 20*this.BossDir;
        }
    },
    // render: function(){
        
    // },
    addEnemy: function() {
        if(this.wave >= 8){
            return;
        }else if(this.wave == 4){
            this.addBoss();
        }else if(this.wave == 2){
            this.rate = 0.4;
        }else if(this.wave == 5){
            this.rate = 0.2;
        }
        console.log(this.wave);
        for(var j=0;j<this.wavesData.enemy.layers[this.wave].length;j++){
            var x=this.wavesData.enemy.layers[this.wave][j];
            var num = this.wavesData.enemy.form[x].n;
            var offset = this.wavesData.enemy.form[x].o;
            for(var i=0;i<num;i++){
                var enemy = this.enemies.getFirstDead();
                if(!enemy){
                    enemy = game.add.sprite(game.width/2, 0, 'sheet', 'enemyBlack1.png');
                    this.enemies.add(enemy);
                }
                enemy.anchor.setTo(0.5, 1);
                enemy.scale.setTo(0.5, 0.5);
                enemy.reset(game.width/2-offset*(num-1)/2+offset*i, -j*80);
                enemy.body.velocity.y = 60;
            }
        }
    },
    addEnemy_1: function() {
        if(this.wave >= 8){
            this.playerDie();
            return;
        }
        for(var j=0;j<this.wavesData.enemy_1.layers[this.wave].length;j++){
            var x=this.wavesData.enemy_1.layers[this.wave][j];
            var num = this.wavesData.enemy_1.form[x].n;
            var offset = this.wavesData.enemy_1.form[x].o;
            for(var i=0;i<num;i++){
                var enemy = this.enemies_1.getFirstDead();
                if(!enemy){
                    enemy = game.add.sprite(game.width/2, 0, 'sheet', 'enemyBlue2.png');
                    this.enemies_1.add(enemy);
                }
                enemy.anchor.setTo(0.5, 1);
                enemy.scale.setTo(0.8, 0.8);
                enemy.reset(game.width/2-offset*(num-1)/2+offset*i, -j*80);
                enemy.body.velocity.y = 60;
            }
        }
        this.wave += 1;
    },
    pause: function() {
        this.click.play();
        if(!this.isPause){
            this.isPause = true;
            this.settingPanel.visible = true;
            this.volumnUp.visible = true;
            this.volumnDown.visible = true;
            this.volumeLabel.visible = true;
            this.BGM.pause();
            game.paused = true;
            game.sound.unsetMute();
        }else{
            this.isPause = false;
            this.settingPanel.visible = false;
            this.volumnUp.visible = false;
            this.volumnDown.visible = false;
            this.volumeLabel.visible = false;
            this.BGM.resume();
            game.paused = false;
        }
    },
    update: function() {
        if(!this.player.inHurt){
            game.physics.arcade.overlap(this.player, this.enemies, this.playerHurt, null, this);
            game.physics.arcade.overlap(this.player, this.enemies_1, this.playerHurt, null, this);
            game.physics.arcade.overlap(this.player, this.enemybeam_1, this.playerHit, null, this);
            game.physics.arcade.overlap(this.player, this.bossBeam, this.playerHit, null, this);
            game.physics.arcade.overlap(this.player, this.superBuff, this.inSuper, null, this);
        }
        game.physics.arcade.overlap(this.player, this.energyBuff, this.addEnergy, null, this);

        /// enemies overlap
        game.physics.arcade.overlap(this.playerbeam, this.enemies, this.enemyHurt, null, this);
        game.physics.arcade.overlap(this.playerbeam_2, this.enemies, this.enemyHurt, null, this);
        game.physics.arcade.overlap(this.multiShot, this.enemies, this.enemyHurt, null, this);
        game.physics.arcade.overlap(this.panel, this.enemies, this.enemyReach, null, this);

        game.physics.arcade.overlap(this.playerbeam, this.enemies_1, this.enemyHurt, null, this);
        game.physics.arcade.overlap(this.playerbeam_2, this.enemies_1, this.enemyHurt, null, this);
        game.physics.arcade.overlap(this.multiShot, this.enemies_1, this.enemyHurt, null, this);
        game.physics.arcade.overlap(this.panel, this.enemies_1, this.enemyReach, null, this);

        game.physics.arcade.overlap(this.playerbeam, this.Boss, this.BossHurt, null, this);
        game.physics.arcade.overlap(this.playerbeam_2, this.Boss, this.BossHurt, null, this);
        game.physics.arcade.overlap(this.multiShot, this.Boss, this.BossHurt, null, this);

        if(this.Boss.body.x <= 200){
            this.Boss.body.velocity.x = 0;
        }
        if(this.Boss.body.x >= 600){
            this.Boss.body.velocity.x = 0;
        }

        if (!this.player.inWorld) { this.playerDie();}
        game.physics.arcade.overlap(this.player, this.panel, this.playerDie, null, this);

        if(this.life <= 0){ this.playerDie(); }
        this.movePlayer();
    },
    enemyHurt: function(beam, enemy) {
        var hit = this.hits_enemy.getFirstDead();
        if(!hit){
            hit = game.add.sprite(beam.x, beam.y-37, 'hitBlue');
            hit.animations.add('explode', [0,1], 15, false);
            this.hits_enemy.add(hit);
        }
        hit.anchor.setTo(0.5, 0.5);
        hit.scale.setTo(0.6, 0.6);
        hit.reset(beam.x, beam.y-37);
        hit.animations.play('explode', 15, false, true);
        this.emitter.x = beam.x;
        this.emitter.y = beam.y;
        this.emitter.start(true, 800, null, 5);
        beam.kill();
        enemy.kill();
        game.global.score += 10;
        this.scoreLabel.setText('Score: '+ game.global.score);
    },
    BossHurt: function(boss,beam) {
        var hit = this.hits_enemy.getFirstDead();
        if(!hit){
            hit = game.add.sprite(beam.x, beam.y-37, 'hitBlue');
            hit.animations.add('explode', [0,1], 15, false);
            this.hits_enemy.add(hit);
        }
        hit.anchor.setTo(0.5, 0.5);
        hit.scale.setTo(0.6, 0.6);
        hit.reset(beam.x, beam.y-37);
        hit.animations.play('explode', 15, false, true);
        this.emitter.x = beam.x;
        this.emitter.y = beam.y;
        this.emitter.start(true, 800, null, 5);
        beam.kill();
        this.BossLife -=1;
        if(this.BossLife <= 0){
            this.Boss.destroy;
            this.playerDie();
        };
    },
    enemyReach: function(panel, enemy) {
        this.lose.play();
        this.reduceLife();
        this.playernotImmune();
        enemy.kill();
    },
    playernotImmune: function() { this.player.inHurt = false; },
    playerHit: function(player, beam) {
        this.playerHurt();
        var hit = this.hits.getFirstDead();
        if(!hit){
            hit = game.add.sprite(beam.x, beam.y+37, 'hitRed');
            hit.animations.add('explode', [0,1], 15, false);
            this.hits.add(hit);
        }
        hit.anchor.setTo(0.5, 0.5);
        hit.scale.setTo(0.6, 0.6);
        hit.reset(beam.x, beam.y+37);
        hit.animations.play('explode', 15, false, true);
        this.emitter_P.x = beam.x;
        this.emitter_P.y = beam.y;
        this.emitter_P.start(true, 800, null, 5);
        beam.kill();
    },
    playerHurt: function() {
        this.reduceLife();
        game.add.tween(this.player).to( { alpha: 0.5 }, 200, Phaser.Easing.Linear.None, true, 0, 3, true).onComplete.add(this.playernotImmune, this);    
    },
    addEnergy: function(player, buff) {
        buff.destroy();
        if(this.energy < 3){
            this.playerEnergy.removeAll();
            this.energy +=1;
            var i = 0;
            for(i = 0; i < this.energy; i++){
                this.playerEnergy.add(game.add.sprite( 500 + 40*i, 550, 'sheet', 'bolt_gold.png'));
            }
        }
    },
    reduceEnergy: function() {
        if(this.energy > 0){
            this.accend.play();
            for(var i=0; i< 11; i++){
                var beam = this.multiShot.getFirstDead();
                if(!beam){
                    beam = game.add.sprite(game.width/2-300 + 60*i, 650, 'sheet', 'laserGreen10.png');
                    this.multiShot.add(beam);
                }
                beam.anchor.setTo(0.5, 1);
                beam.reset(game.width/2-300 + 60*i,this.player.y - 19);
                beam.checkWorldBounds = true;
                beam.outOfBoundsKill = true;
                game.add.tween(beam).to( { alpha: 0 }, 500, null, true, 0, 3,true);
            }
            this.accend.onStop.add(function() {
                this.skillshot.play();
                this.multiShot.setAll('body.velocity.y', -600);  
            }, this);
            this.playerEnergy.removeAll();
            this.energy -= 1;
            var i = 0;
            for(i = 0; i < this.energy; i++){
                this.playerEnergy.add(game.add.sprite( 500 + 40*i, 550, 'sheet', 'bolt_gold.png'));
            }
            game.add.tween(game.add.sprite( 85+40*i, 560, 'sheet', 'bolt_gold.png')).to( { alpha: 0 }, 500).start();
        }
    },
    
    reduceLife: function() {
        this.life -= 1;
        if(this.life > 0){
            this.player.inHurt = true;
            this.playerLife.removeAll();
            var i = 0;
            for(i = 0; i < this.life; i++){
                this.playerLife.add(game.add.sprite( 85+40*i, 560, 'sheet', 'playerLife1_red.png'));
            }
            game.add.tween(game.add.sprite( 85+40*i, 560, 'sheet', 'playerLife1_red.png')).to( { alpha: 0 }, 500).start();
        }else{
            this.playerDie();
        }
    },
    playerDie: function() {
        this.BGM.stop();
        game.state.start('end');
    },
    movePlayer: function() {
        if (this.cursor.left.isDown) {
            this.player.body.x += -5;
        }
        else if (this.cursor.right.isDown) { 
            this.player.body.x += 5;
        }
        else if (this.cursor.up.isDown) { 
            this.player.body.y += -5;
        }  
        else if (this.cursor.down.isDown) { 
            this.player.body.y += 5;
        }
    },
    playerShoot: function() {
        this.laser2.play();
        var beam = this.playerbeam.getFirstDead();
        if(!beam){
            beam = game.add.sprite(this.player.x, this.player.y, 'sheet', 'laserBlue01.png');
            this.playerbeam.add(beam);
        }
        beam.anchor.setTo(0.5, 1);
        beam.reset(this.player.x, this.player.y - 19);
        beam.body.velocity.y = -300;
        beam.checkWorldBounds = true;
        beam.outOfBoundsKill = true;
    },
    playerShoot_2: function() {
        this.laser2.play();
        var beam = this.playerbeam_2.getFirstDead();
        if(!beam){
            beam = game.add.sprite(this.player.x, this.player.y, 'sheet', 'laserBlue07.png');
            this.playerbeam_2.add(beam);
        }
        beam.anchor.setTo(0.5, 1);
        beam.reset(this.player.x+25, this.player.y - 19);
        beam.body.velocity.y = -300;
        beam.checkWorldBounds = true;
        beam.outOfBoundsKill = true;
        var beam2 = this.playerbeam_2.getFirstDead();
        if(!beam2){
            beam2 = game.add.sprite(this.player.x, this.player.y, 'sheet', 'laserBlue07.png');
            this.playerbeam_2.add(beam2);
        }
        beam2.anchor.setTo(0.5, 1);
        beam2.reset(this.player.x-25, this.player.y - 19);
        beam2.body.velocity.y = -300;
        beam2.checkWorldBounds = true;
        beam2.outOfBoundsKill = true;
    },
    enemyShoot_1: function() {
        this.enemies_1.forEachAlive(function(enemy) {
            if(game.rnd.frac() >= this.rate){
                this.laser1.play();
                var beam = this.enemybeam_1.getFirstDead();
                if(!beam){
                    beam = game.add.sprite(enemy.x, enemy.y, 'sheet', 'laserRed05.png');
                    this.enemybeam_1.add(beam);
                }
                beam.anchor.setTo(0.5, 0);
                beam.reset(enemy.x, enemy.y);
                beam.body.velocity.y = 200;
                beam.checkWorldBounds = true;
                beam.outOfBoundsKill = true;
            }
        },this);
        /// do {var enemy = this.enemies_1.getRandom();} while (enemy.alive === true)
    }
};