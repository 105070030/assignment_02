var menuState = {
    create: function(){

        //Add a background image
        game.add.image(0,0,'menu');

        var nameLabel = game.add.text(game.width/2, game.height/2, 'Space Shooter', {font:'60px future', fill: '#000000'});
        nameLabel.anchor.setTo(0.5,0.5);
        //nameLabel.fontWeight = 'bold';
        nameLabel.stroke = '#ffffff';
        nameLabel.strokeThickness = 15;

        //Show the score at the center of the screen
        // var scoreLabel = game.add.text(game.width/2, , 'score: ' + game.global.score, {font: '25px future', fill: '#ffffff'});
        // scoreLabel.anchor.setTo(0.5, 0.5);


        //Explain how to start the game
        var startLabel = game.add.text(game.width/2, game.height/2+80, 'press UP', {font: '25px future', fill: '#ffffff'});
        startLabel.anchor.setTo(0.5, 0.5);
        
        var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
        upKey.onDown.add(this.start, this);
    },
    start: function() {
        // Start the actuall game
        game.state.start('play');
    }
}