# Software Studio 2019 Spring Assignment 2
https://105070030.gitlab.io/assignment_02/

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|Y|

## Website Detail Description
1.按上鍵開始遊戲
2.使用上下左右鍵移動角色
3.若取得雷電可按C鍵使用技能
4.點擊右下方設定按鈕可暫停調整音量
5.結束畫面新高分時輸入名字後按ENTER即可登錄

# Basic Components Description : 
1. Jucify mechanisms : 角色受攻擊閃爍
2. Animations : 角色全能狀態顏色變換
3. Particle Systems : 子彈具粒子效果
4. Sound effects : 有背景音樂、射擊音效、按鈕音效等
5. Leaderboard : 結束畫面有RANKING按鈕

# Bonus Functions Description : 
1.Boss: 隨機移動
2.Unique Bullet : Boss發散式子彈、角色技能十一連發、角色全能狀態三管發射
3. [xxxx] : [xxxx]